import express from "express";
import bodyParser from "body-parser";
import axios from 'axios'
import dotenv from 'dotenv'
dotenv.config();

const app = express();
const port = process.env.PORT || 3000;
app.use(bodyParser.json());

app.get("/", async (req, res) => {
  try {
    res.status(200).send("webhooks working correctly");
  } catch (error) {
    console.error("webhook not working", error.message);
    res.status(500).send("Internal Server Error");
  }
});

// Endpoint to receive webhook notifications from GitLab
app.post("/webhook", async (req, res) => {
  try {
    // Extract relevant information from the webhook payload
    const { project, object_attributes } = req.body;
    // Extract project ID and merge request information
    const projectId = project.id;
    const mergeRequest = {
      iid: object_attributes.iid,
      source_branch: object_attributes.source_branch,
      target_branch: object_attributes.target_branch,
      title: object_attributes.title,
      url: object_attributes.url,
      last_commit: object_attributes.last_commit,
      changes: {},
    };

    mergeRequest.changes =await getCodeDifference(projectId, mergeRequest);
    // Call ChatGPT API to generate code review comments
    const reviewComments = await generateCodeReviewComments(
      mergeRequest
    );

    // Post comments to GitLab
    await postCommentsToGitLab(projectId, mergeRequest, reviewComments);

    res.status(200).send("Webhook received successfully");
  } catch (error) {
    console.error("Error processing webhook:", error.message);
    res.status(500).send("Internal Server Error");
  }
});

// Function to generate code review comments using ChatGPT
const generateCodeReviewComments = async (mergeRequest) =>{
  try {
    // Assuming you have an API key for OpenAI GPT API stored in an environment variable
    const openaiApiKey = process.env.OPENAI_API_KEY;
    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: "https://api.openai.com/v1/chat/completions",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${openaiApiKey}`,
      },
      data: {
        model: "gpt-3.5-turbo",
        messages: [
          {
            role: "user",
            content:
              "Review the following code changes:\n" + JSON.stringify(mergeRequest.changes),
          },
        ],
      },
    };
    // Example code to call OpenAI GPT API
    const response = await axios.request(config);

    // Extract and return the generated comments
    return response.data?.choices?.[0].message?.content?.trim();
  } catch (error) {
    console.error("Error generating code review comments:", error.message);
    throw error;
  }
}


// Function to post comments to GitLab
 const postCommentsToGitLab = async (projectId, mergeRequest, comments) => {
  try {
    // Assuming you have a GitLab access token stored in an environment variable
    const gitlabAccessToken = process.env.GITLAB_ACCESS_TOKEN;
    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequest.iid}/discussions?body=${comments}`,
      headers: {
        "PRIVATE-TOKEN": gitlabAccessToken,
      },
    };

    await axios
      .request(config)
      .then((response) => {
        console.log("success");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.error("Error posting comments to GitLab:", error.message);
  }
}

// Function to get code difference
 const getCodeDifference = async (projectId, mergeRequest) => {
  try {
    // Assuming you have a GitLab access token stored in an environment variable
    const gitlabAccessToken = process.env.GITLAB_ACCESS_TOKEN;
    const config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequest.iid}/changes`,
      headers: {
        "PRIVATE-TOKEN": gitlabAccessToken,
      },
    };

   const response =  await axios.request(config)
   return response.data.changes?.[0]?.diff || {}
  } catch (error) {
    console.error("Error posting comments to GitLab:", error.message);
    return {}
  }
}

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
